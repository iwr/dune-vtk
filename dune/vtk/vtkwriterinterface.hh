#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtkwriterinterface.hh and VtkWriterInterface are deprecated. Use vtkwriterbase.hh and Vtk::VtkWriterBase instead."
#endif

#include <dune/vtk/vtkwriterbase.hh>

namespace Dune
{
  template <class GV, class DC>
  class [[deprecated("Use Vtk::VtkWriterInterface instead.")]] VtkWriterInterface
    : public Vtk::VtkWriterBase<GV,DC>
  {
    using Base = Vtk::VtkWriterBase<GV,DC>;
  public:
    using Base::Base;
  };

} // end namespace Dune
