#pragma once

#include <cstdint>
#include <vector>

#include <dune/grid/common/partitionset.hh>
#include <dune/vtk/datacollectorinterface.hh>

namespace Dune::Vtk
{
  struct Cells
  {
    std::vector<std::uint8_t> types;
    std::vector<std::int64_t> offsets;
    std::vector<std::int64_t> connectivity;
  };

  // tag to indicate whether point IDs should be written
  template <bool = true>
  struct WritePointIDs {};


  template <class GridView, class Derived, class Partition = Partitions::InteriorBorder>
  class UnstructuredDataCollectorInterface
      : public DataCollectorInterface<GridView, Derived, Partition>
  {
    using Super = DataCollectorInterface<GridView, Derived, Partition>;

  public:
    using Super::dim;
    using Super::partition;

  public:
    UnstructuredDataCollectorInterface (GridView const& gridView)
      : Super(gridView)
    {}

    template <bool b>
    UnstructuredDataCollectorInterface (GridView const& gridView, WritePointIDs<b>)
      : Super(gridView)
      , writePointIDs_(b)
    {}

    /// \brief Return cell types, offsets, and connectivity. \see Cells
    Cells cells () const
    {
      return this->asDerived().cellsImpl();
    }

    /// \brief Return parallel unique IDs for each point in the grid.
    std::vector<std::uint64_t> pointIds () const
    {
      if (writePointIDs_)
        return this->asDerived().pointIdsImpl();
      else
        return std::vector<std::uint64_t>{};
    }

  protected:
    // default implementation
    std::vector<std::uint64_t> pointIdsImpl () const
    {
      return {};
    }

  private:
    bool writePointIDs_ = false;
  };

} // end namespace Dune::Vtk
