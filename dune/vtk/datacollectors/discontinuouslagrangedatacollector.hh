#pragma once

#include <cassert>
#include <map>
#include <vector>

#include <dune/geometry/referenceelements.hh>
#include <dune/grid/common/partitionset.hh>
#include <dune/grid/common/mcmgmapper.hh>
#include <dune/vtk/types.hh>
#include <dune/vtk/utility/lagrangepoints.hh>

#include "unstructureddatacollector.hh"

namespace Dune::Vtk
{
  /**
   * \brief Implementation of \ref DataCollector for Lagrange cells
   *
   * In contrast to \ref LagrangeDataCollector this one is
   * discontinuous across cells and will create one copy
   * per cell for each vertex. This \ref DataCollector is
   * particularly suited for writing higher order discontinuous
   * polynomial data.
   */
  template <class GridView, int ORDER = -1>
  class DiscontinuousLagrangeDataCollector
      : public UnstructuredDataCollectorInterface<GridView, DiscontinuousLagrangeDataCollector<GridView,ORDER>, Partitions::All>
  {
    using Self = DiscontinuousLagrangeDataCollector;
    using Super = UnstructuredDataCollectorInterface<GridView, Self, Partitions::All>;

    auto pointSetMapper () const
    {
      auto pointSetLayout = [&](Dune::GeometryType gt, int dimgrid) -> unsigned int {
        if (int(gt.dim()) == dimgrid)
          return pointSets_.at(gt).size();
        else
          return 0;
      };
      return Dune::MultipleCodimMultipleGeomTypeMapper(gridView(), pointSetLayout);
    }

  public:
    static_assert(ORDER != 0, "Order 0 not supported");
    using Super::dim;
    using Super::partition; // NOTE: Lagrange data-collector currently implemented for the All partition only
    using Super::gridView;

  public:
    DiscontinuousLagrangeDataCollector (GridView const& gridView, int order = ORDER)
      : Super(gridView)
      , order_(order)
    {
      assert(order > 0 && "Order 0 not supported");
      assert(ORDER < 0 || order == ORDER);
    }

    /// Construct the point sets
    void updateImpl ()
    {
      auto const& indexSet = gridView().indexSet();

      pointSets_.clear();
      for (auto gt : indexSet.types(0))
        pointSets_.emplace(gt, order_);

      for (auto& pointSet : pointSets_)
        pointSet.second.build(pointSet.first);

      numPoints_ = pointSetMapper().size();
    }

    /// Return number of Lagrange nodes
    std::uint64_t numPointsImpl () const
    {
      return numPoints_;
    }

    /// Return a vector of point coordinates.
    /**
    * The vector of point coordinates is composed of vertex coordinates first and second
    * edge center coordinates.
    **/
    template <class T>
    std::vector<T> pointsImpl () const
    {
      std::vector<T> data(this->numPoints() * 3);
      auto const& mapper = pointSetMapper();

      for (auto const& element : elements(gridView(), partition)) {
        auto geometry = element.geometry();

        auto const& pointSet = pointSets_.at(element.type());

        for (std::size_t i = 0; i < pointSet.size(); ++i) {
          auto const& p = pointSet[i];
          std::size_t idx = 3 * (mapper.index(element) + i);

          auto v = geometry.global(p.point());
          for (std::size_t j = 0; j < v.size(); ++j)
            data[idx + j] = T(v[j]);
          for (std::size_t j = v.size(); j < 3u; ++j)
            data[idx + j] = T(0);
        }
      }
      return data;
    }

    /// Return number of grid cells
    std::uint64_t numCellsImpl () const
    {
      return gridView().size(0);
    }

    /// \brief Return cell types, offsets, and connectivity. \see Cells
    /**
    * The cell connectivity is composed of cell vertices first and second cell edges,
    * where the indices are grouped [vertex-indices..., (#vertices)+edge-indices...]
    **/
    Cells cellsImpl () const
    {
      Cells cells;
      cells.connectivity.reserve(this->numPoints());
      cells.offsets.reserve(this->numCells());
      cells.types.reserve(this->numCells());

      auto const& mapper = pointSetMapper();


      std::int64_t old_o = 0;
      for (auto const& element : elements(gridView(), partition)) {
        Vtk::CellType cellType(element.type(), Vtk::CellType::LAGRANGE);

        auto const& pointSet = pointSets_.at(element.type());

        for (std::size_t i = 0; i < pointSet.size(); ++i) {
          std::size_t idx = (mapper.index(element) + i);
          cells.connectivity.push_back(std::int64_t(idx));
        }

        cells.offsets.push_back(old_o += pointSet.size());
        cells.types.push_back(cellType.type());
      }
      return cells;
    }

    /// Evaluate the `fct` at element vertices and edge centers in the same order as the point coords.
    template <class T, class GlobalFunction>
    std::vector<T> pointDataImpl (GlobalFunction const& fct) const
    {
      int nComps = fct.numComponents();
      std::vector<T> data(this->numPoints() * nComps);
      auto const& mapper = pointSetMapper();

      auto localFct = localFunction(fct);
      for (auto const& element : elements(gridView(), partition)) {
        localFct.bind(element);

        auto const& pointSet = pointSets_.at(element.type());

        for (std::size_t i = 0; i < pointSet.size(); ++i) {
          auto const& p = pointSet[i];
          std::size_t idx = nComps*(mapper.index(element) + i);

          for (int comp = 0; comp < nComps; ++comp)
            data[idx + comp] = T(localFct.evaluate(comp, p.point()));
        }
        localFct.unbind();
      }
      return data;
    }

  private:
    unsigned int order_;
    std::uint64_t numPoints_ = 0;

    using PointSet = LagrangePointSet<typename GridView::ctype, GridView::dimension>;
    std::map<GeometryType, PointSet> pointSets_;
  };

} // end namespace Dune::Vtk
