#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtkunstructuredgridwriter.hh and VtkUnstructuredGridWriter are deprecated. Use unstructuredgridwriter.hh and Vtk::UnstructuredGridWriter instead."
#endif

#include <dune/vtk/writers/unstructuredgridwriter.hh>

namespace Dune
{
  template <class GridView, class DataCollector = Vtk::ContinuousDataCollector<GridView>>
  class [[deprecated("Use Vtk::UnstructuredGridWriter instead.")]] VtkUnstructuredGridWriter
    : public Vtk::UnstructuredGridWriter<GridView,DataCollector>
  {
    using Base = Vtk::UnstructuredGridWriter<GridView,DataCollector>;
  public:
    using Base::Base;
  };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  // deduction guides
  template <class GridView, class... Args,
    Vtk::IsGridView<GridView> = true>
  VtkUnstructuredGridWriter(GridView, Args...)
    -> VtkUnstructuredGridWriter<GridView, Vtk::ContinuousDataCollector<GridView>>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkUnstructuredGridWriter(DataCollector&, Args...)
    -> VtkUnstructuredGridWriter<typename DataCollector::GridView, DataCollector>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkUnstructuredGridWriter(std::shared_ptr<DataCollector>, Args...)
    -> VtkUnstructuredGridWriter<typename DataCollector::GridView, DataCollector>;
#pragma GCC diagnostic pop

} // end namespace Dune
