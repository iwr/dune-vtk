#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtkrectilineargridwriter.hh and VtkRectilinearGridWriter are deprecated. Use rectilineargridwriter.hh and Vtk::RectilinearGridWriter instead."
#endif

#include <dune/vtk/writers/rectilineargridwriter.hh>

namespace Dune
{
  template <class GridView, class DataCollector = Vtk::StructuredDataCollector<GridView>>
  class [[deprecated("Use Vtk::RectilinearGridWriter instead.")]] VtkRectilinearGridWriter
    : public Vtk::RectilinearGridWriter<GridView,DataCollector>
  {
    using Base = Vtk::RectilinearGridWriter<GridView,DataCollector>;
  public:
    using Base::Base;
  };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  // deduction guides
  template <class GridView, class... Args,
    Vtk::IsGridView<GridView> = true>
  VtkRectilinearGridWriter(GridView, Args...)
    -> VtkRectilinearGridWriter<GridView, Vtk::StructuredDataCollector<GridView>>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkRectilinearGridWriter(DataCollector&, Args...)
    -> VtkRectilinearGridWriter<typename DataCollector::GridView, DataCollector>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkRectilinearGridWriter(std::shared_ptr<DataCollector>, Args...)
    -> VtkRectilinearGridWriter<typename DataCollector::GridView, DataCollector>;
#pragma GCC diagnostic pop

} // end namespace Dune
