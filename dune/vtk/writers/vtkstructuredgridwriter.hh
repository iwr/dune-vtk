#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtkstructuredgridwriter.hh and VtkStructuredGridWriter are deprecated. Use structuredgridwriter.hh and Vtk::StructuredGridWriter instead."
#endif

#include <dune/vtk/writers/structuredgridwriter.hh>

namespace Dune
{
  template <class GridView, class DataCollector = Vtk::StructuredDataCollector<GridView>>
  class [[deprecated("Use Vtk::StructuredGridWriter instead.")]] VtkStructuredGridWriter
    : public Vtk::StructuredGridWriter<GridView,DataCollector>
  {
    using Base = Vtk::StructuredGridWriter<GridView,DataCollector>;
  public:
    using Base::Base;
  };

#pragma GCC diagnostic push
#pragma GCC diagnostic ignored "-Wdeprecated-declarations"
  // deduction guides
  template <class GridView, class... Args,
    Vtk::IsGridView<GridView> = true>
  VtkStructuredGridWriter(GridView, Args...)
    -> VtkStructuredGridWriter<GridView, Vtk::StructuredDataCollector<GridView>>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkStructuredGridWriter(DataCollector&, Args...)
    -> VtkStructuredGridWriter<typename DataCollector::GridView, DataCollector>;

  template <class DataCollector, class... Args,
    Vtk::IsDataCollector<DataCollector> = true>
  VtkStructuredGridWriter(std::shared_ptr<DataCollector>, Args...)
    -> VtkStructuredGridWriter<typename DataCollector::GridView, DataCollector>;
#pragma GCC diagnostic pop

} // end namespace Dune
