#pragma once

#include <dune/common/fvector.hh>
#include <dune/common/dynvector.hh>
#include <dune/functions/common/defaultderivativetraits.hh>

namespace Dune::Vtk
{
  /// Context indicating that a GridFunction generates a local-function from point data
  struct PointContext {};

  /// Context indicating that a GridFunction generates a local-function from cell data
  struct CellContext {};

  /// \brief Type-Traits to associate a GridFunction to a GridCreator.
  /**
    * Each GridCreator type should specialize this template and set `type` to the
    * corresponding GridFunction type, e.g. Vtk::ContinuousGridFunction or
    * Vtk::LagrangeGridFunction.
    *
    * \tparam GridCreator   A Type implementing the GridCreatorInterface
    * \tparam Range         Range type of the data extracted from the file.
    * \tparam Context       A context-type for specialization of the local-function, e.g.,
    *                       Vtk::PointContext or Vtk::CellContext.
    **/
  template <class GridCreator, class Range, class Context>
  struct AssociatedGridFunction
  {
    using type = void;
  };


  template <class Element, class Range, class Domain>
  struct DummyLocalFunctionDerivative
  {
    using DerivativeRange = typename Functions::DefaultDerivativeTraits<Range(Domain)>::Range;
    void bind (Element const& element) { /* do nothing */ };
    void unbind () { /* do nothing */ }
    DerivativeRange operator() (Domain const& local) const
    {
      DUNE_THROW(Dune::NotImplemented,
        "The derivative is not implemented.");
    }
  };

  namespace Impl
  {
    template <class Range>
    struct ConstructRange
    {
      template <class Field>
      static Range apply (int size, Field const& value);
    };


    template <class Range, class Field>
    Range constructRange (int size, Field const& value)
    {
      return ConstructRange<Range>::apply (size,value);
    }

    template <class T, int n>
    struct ConstructRange<Dune::FieldVector<T,n>>
    {
      template <class Field>
      static Dune::FieldVector<T,n> apply ([[maybe_unused]] int size, Field const& value)
      {
        assert(size == n);
        return Dune::FieldVector<T,n>(value);
      }
    };

    template <class T>
    struct ConstructRange<Dune::DynamicVector<T>>
    {
      template <class Field>
      static Dune::DynamicVector<T> apply (int size, Field const& value)
      {
        return Dune::DynamicVector<T>(size,T(value));
      }
    };


    template <class Range>
    struct ResizeRange
    {
      static void apply (Range& range, int size);
    };


    template <class Range>
    void resizeRange (Range& range, int size)
    {
      ResizeRange<Range>::apply(range,size);
    }

    template <class T, int n>
    struct ResizeRange<Dune::FieldVector<T,n>>
    {
      static void apply ([[maybe_unused]] Dune::FieldVector<T,n>& range, [[maybe_unused]] int size)
      {
        assert(n == size);
      }
    };

    template <class T>
    struct ResizeRange<Dune::DynamicVector<T>>
    {
      static void apply (Dune::DynamicVector<T>& range, int size)
      {
        range.resize(size);
      }
    };

  } // end namespace Impl

} // end namespace Dune::Vtk
