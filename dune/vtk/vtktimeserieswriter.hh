#pragma once

#ifndef DUNE_VTK_DISABLE_DEPRECATION_WARNING
#warning "File vtktimeserieswriter.hh and VtkTimeseriesWriter are deprecated. Use timeserieswriter.hh and Vtk::TimeseriesWriter instead."
#endif

#include <dune/vtk/timeserieswriter.hh>

namespace Dune
{
  template <class VtkWriter>
  class [[deprecated("Use Vtk::TimeseriesWriter instead.")]] VtkTimeseriesWriter
    : public Vtk::TimeseriesWriter<VtkWriter>
  {
    using Base = Vtk::TimeseriesWriter<VtkWriter>;
  public:
    using Base::Base;
  };

} // end namespace Dune
