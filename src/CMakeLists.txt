set(GRID_PATH "${CMAKE_CURRENT_SOURCE_DIR}/../doc/")

# link all tests against Dune::Vtk
link_libraries(Dune::Vtk)

dune_add_test(NAME dunevtk_vtkreader
              SOURCES vtkreader.cc)

dune_add_test(NAME dunevtk_legacyvtkwriter
              SOURCES legacyvtkwriter.cc)

dune_add_test(NAME dunevtk_vtkwriter
              SOURCES vtkwriter.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_benchmark
              SOURCES benchmark.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_datacollector
              SOURCES datacollector.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_datareader
              SOURCES datareader.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_dunegridvtkreader
              SOURCES dunegridvtkreader.cc
              CMAKE_GUARD "dune-uggrid_FOUND OR dune-alugrid_FOUND")

dune_add_test(NAME dunevtk_lagrangepoints
              SOURCES lagrangepoints.cc)

dune_add_test(NAME dunevtk_lagrangereader
              SOURCES lagrangereader.cc
              COMPILE_DEFINITIONS "GRID_PATH=\"${GRID_PATH}\""
              CMAKE_GUARD dune-foamgrid_FOUND)

dune_add_test(NAME dunevtk_structuredgridwriter
              SOURCES structuredgridwriter.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_geometrygrid
              SOURCES geometrygrid.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_timeserieswriter
              SOURCES timeserieswriter.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_pvdwriter
              SOURCES pvdwriter.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_vectorwriter
              SOURCES vectorwriter.cc
              CMAKE_GUARD dune-functions_FOUND)

dune_add_test(NAME dunevtk_codimwriter
              SOURCES codimwriter.cc
              CMAKE_GUARD dune-functions_FOUND)

if (dune-polygongrid_FOUND)
  # CMAKE_GUARD can not be used, since a dummy target is created and linked against dunepolygongrid
  dune_add_test(NAME dunevtk_polygongrid
                SOURCES polygongrid.cc
                LINK_LIBRARIES dunepolygongrid)
endif ()

add_subdirectory(test)
